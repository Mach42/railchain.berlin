# RailChain Website

Dieses Repository enthält die Quellen für unsere Webseite, die mit dem Templatesystem Jekyll generiert wird.

Alle Inhaltsseiten lassen sich in Markdown bzw. als HTML editieren.
 - [Homepage](index.html) (HTML)
 - [Asset Identity](asset_identity.md), [Data Logger](data_logger.md), [Juridical Recorder](juridical_recorder.md) (Markdown)


Zusätzliche Seiten können einfach hinzugefügt werden, indem eine neue `.md` Datei erstellt wird.
"Posts", also News die auch auf der Homepage erscheinen, können im Ordner [_posts](_posts) angelegt werden.

**Tipp fürs Bearbeiten**

Wer während des Editierens eine Vorschau von der Seite bekommen möchte, kann das Repository klonen und den [Docker Development Container](https://code.visualstudio.com/docs/remote/containers) für Visual Studio Code benutzen.

Darin kann ein Webserver mit `bundle exec jekyll serve` gestartet werden.

Über Ctrl-Klick auf die im Konsolenfenster angezeigte Adresse (Beispiel: `Server address: http://127.0.0.1:4000/`) öffnet sich ein Browserfenster.

Alternativ lässt sich Jekyll als Ruby gem auch nativ installieren und aufrufen.
