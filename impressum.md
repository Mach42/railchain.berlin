---
layout: page
title: Impressum und Datenschutzerklärung
permalink: /impressum/
hide_from_menu: true
---
Für sämtliche interne Inhalte des Webauftritts sind die Konsortialpartner des RailChain-Projektes verantwortlich. Bei Kritik oder Anmerkungen zu den Inhalten, wenden Sie sich bitte an den Ansprechpartner Ingo Schwarzer (ingo.schwarzer@deutschebahn.com)

Ingo Schwarzer<br/>
Chief Digitalist<br/>
Fellow

DB Systel GmbH<br/>
Marktstraße 8<br/>
10317 Berlin<br/>
Tel. +49 30 297 16370<br/>
Email: ingo.schwarzer@deutschebahn.com

Registergericht Frankfurt am Main HRB 78 707

#### Urheberrecht
Diese Website und deren Inhalte unterliegen dem deutschen Leistungsschutz- und Urheberrecht. Unerarlaubte Verfielfältigungen außerhalb der engen Grenzen des UrhG sind ohne die schriftliche Zustimmung des Konsortiums verboten.

#### Haftung für die abrufbaren Inhalte
Die hier abrufbaren Inhalte erheben keinen Anspruch auf Korrektheit, Verwendbarkeit, und/oder Vollständigkeit. Die rechtlichen Hinweise sind unverbindlich und daher nicht rechstbindend. Sie haben vielmehr einen informativen Charakter, ohne dass für die Richtigkeit Gewähr übernommen wird.

#### Haftung für Links
Die im Rahmen dieses Angebotes weiterverweisenden externen Links zu Angeboten Dritter führen zu Inhalten der jeweiligen Anbieter und sind nicht die des Konsortiums. Diese Verweise sind als solche aus dem Zusammenhang erkennbar oder gesondert gekennzeichnet. Das Konsortium hat keinerlei Einfluss auf die dort bereitgehaltenen Inhalte und macht sich diese durch die Verweise nicht zu eigen.

<!-- ## Bildnachweis
Das Copyright für Bilder liegt, soweit nicht anders vermerkt, bei der TODO. -->

## Datenschutzerklärung
Das Projekt RailChain freut sich über ihren Besuch auf der Website und Ihr Interesse an unserem Projekt. Der Schutz Ihrer personenbezogenen Daten ist uns ein wichtiges Anliegen. Wir möchten, dass Sie sich beim Besuch unserer Internetseite sicher fühlen. Ihre Daten werden von uns weder veröffentlicht noch an Dritte weitergegeben. Da Sie ein Recht auf Auskunft über jegliche Nutzung Ihrer Daten haben, informieren Sie diese Hinweise zum Datenschutz darüber, wann wir welche Daten speichern und wie wir sie verwenden - selbstverständlich unter Beachtung der geltenden deutschen Datenschutzvorschriften. Zum Schutz Ihrer Rechte haben wir technische und organisatorische Maßnahmen getroffen.

#### Datenspeicherung
In Verbindung mit Ihrem Zugriff werden in unserem Server für Zwecke der Datensicherheit vorübergehend Daten gespeichert. Jeder Datensatz besteht aus:

 - dem Namen der angeforderten Datei
 - der anonymisierten IP-Adresse des Rechners, der die Datei anforderte
 - dem Datum und der Uhrzeit der Anforderung
 - der übertragenen Datenmenge
 - dem Zugriffsstatus (Datei übertragen, Datei nicht gefunden, etc.)
 - einer Beschreibung des Typs des verwendeten Webbrowsers

Diese gespeicherten Daten werden ausschließlich zu statistischen Zwecken ausgewertet und auf keinen Fall an Dritte weitergeleitet.
