---
layout: page
title: Data Logger
permalink: /data_logger/
hide_from_menu: true
---

Der zweite Use Case ist die Implementierung eines Daten-Loggers ohne Echtzeitanforderungen.
Hierbei sollen Daten wie z. B. Verspätungsinformationen oder Sensormesswerte und Diagnosemeldungen erfasst und zuverlässig und nicht manipulierbar auf einer dezentralen Blockchain abgespeichert werden.
Dabei wird zusätzlich sichergestellt, dass die einzelnen Informationen und Komponenten keine unterschiedlichen, abweichenden Zustände melden (z. B. Nachrichten in unterschiedlicher Reihenfolge loggen), sondern Daten erst dann in der Blockchain hinterlegt werden, wenn ihre Ordnung und Echtheit sichergestellt werden kann.
